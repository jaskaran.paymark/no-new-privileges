#!/usr/bin/env python3
import sys

if len(sys.argv) > 1:
    DOCKERFILE=sys.argv[1]
else:
    sys.exit("[e] Usage: "+sys.argv[0]+"[Dockerfile path]")

if 'no-new-privileges:true' not in open(DOCKERFILE).read():
    sys.exit("[e] Process in the container can gain additional privileges")
